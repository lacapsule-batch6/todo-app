describe('Tests application Todo', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000'); // Remplacez par l'URL de votre app si différente
    cy.get("button").click(); // Login
  });

  it('Devrait permettre ajout 3 todo', () => {
    cy.contains('Add Todo'); // Verifie l'affichage du titre
    cy.get('input[id="title"]').type('Pommes{enter}'); // Entrer 1er Todo dans la liste
    cy.wait(3000);
    cy.contains('Pommes'); // Verifie l'ajout du 1er Todo
    cy.get('input[id="title"]').type('Poires{enter}'); // Entrer 2eme Todo dans la liste
    cy.wait(3000);
    cy.contains('Poires'); // Verifie l'ajout du 2eme Todo
    cy.get('input[id="title"]').type('Bananes{enter}'); // Entrer 3eme Todo dans la liste
    cy.wait(3000);
    cy.contains('Bananes'); // Verifie l'ajout du 3eme Todo

    cy.contains('Total Todos: 3'); // Verifie le nombre de Todos total

    cy.contains('Poires').click(); // Selectionne un todo
    cy.contains('Selected Todos: 1'); // Verifie le nombre de Todos selectionnés
  });
});