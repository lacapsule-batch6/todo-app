# todo-app

This project is for educationnal purpose only - Made with ❤️ and lots of coffee!
 
Hey! If you're reading this, know that this code is COMPLETELY free of rights.
You can copy it, modify it, distribute it, or even sing it a lullaby but please don't delete this README file.
 
However, a little shoutout or thank you would be appreciated if this code helps you out.
Any financial contribution would also encourage me to maintain the code
You may send any crypto to 0x432428923B4F06c10E8A5a98044D09A2DFCa5Ee5
But nothing is mandatory...
## Licence

MIT License  ![MIT License](https://img.shields.io/badge/License-MIT-green.svg)

Copyright (c) 2024 Frederic LANEQUE 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
## Authors

- [@fredlaneque](https://www.github.com/fredlaneque)

## Roadmap

+ Your suggestions are welcome...
## Challenges

SETUP

Vous allez construire des tests e2e et automatiser l’exécution de ces tests dans une pipeline GitLab sur une simple application de to-do list.

Pour intégrer Cypress à votre système d’intégration continue sur GitLab, consultez la documentation de Cypress :
https://docs.cypress.io/guides/continuous-integration/gitlab-ci

⚠️ Attention : GitLab limite l’usage de ses outils CI/CD à 400 minutes dans sa version gratuite.
Évitez les pushs inutiles et vérifiez bien que les jobs ne durent pas plus de quelques minutes (dans l’onglet CI/CD > Jobs) et stoppez les jobs qui ne sont pas nécessaires.

👉 Clonez le repository suivant sur GitLab : https://gitlab.com/la-capsule-bootcamp/todo-app

👉 Exécutez les commandes suivantes afin d’installer les dépendances et lancer l’application.
```bash
cd todo-app
yarn install
yarn start
```
👉 Arrêtez le projet (via Ctrl + C) puis supprimez la liaison entre le dépôt local et le dépôt distant.

👉 Liez votre dépôt local à un nouveau repository GitLab que vous aurez créé au préalable.

CRÉATION DES TESTS

👉 Lancez Cypress directement depuis le terminal.
```bash
yarn run cypress:open
```
👉 Créez des tests simples (un “it” par test) qui s’assurent que les features principales de la todo app fonctionnent:
Ajout d’une todo et affichage de celle-ci
Compteur du nombre de todos
Sélection d’une todo et actualisation automatique du compteur de todos sélectionnées

👉 Lancez ces tests en local avant de passer à l’étape suivante.

## This project started

Created 31-01-2024 at 14:36:34 tested using Python 3.12"
